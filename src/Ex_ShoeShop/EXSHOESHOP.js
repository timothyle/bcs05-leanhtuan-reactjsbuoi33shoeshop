import React, { Component } from 'react'
import { dataShoe } from './datashoeList';
import DetailShoe from './DetailShoe';
import Cart from './Cart';
import ListShoe from './ListShoe';

export default class EXSHOESHOP extends Component {
state = {
    shoeArr: dataShoe,
    detail: dataShoe[1],
    cart:[],
}
handleChangeDetail = (value) => {
  this.setState({ detail: value });
};
handleAddToCart = (shoe) => {
  let cloneCart = [...this.state.cart];
  let index = this.state.cart.findIndex((item) => {
    return item.id === shoe.id;
  });
  if (index === -1) {
    // th2: sp chưa có trong giỏ hàng
    let cartItem = { ...shoe, number: 1 };
    cloneCart.push(cartItem);
  } else {
    // th1 đã có
    cloneCart[index].number++;
  }
  this.setState({
    cart: cloneCart,
  });
  // th1 : sp đã có trong giỏi hàng => tăng key number
  // th2 : sp chưa có => tạo mới và push
};
handleUpdateQuantity = (data,value) => {
  let cloneCart = [...this.state.cart];
  let index = this.state.cart.findIndex((item) => {
    return item.id === data;
  });
  if (cloneCart[index].number === 1 && value === -1) {
    cloneCart.splice(index, 1);
  } else {
    cloneCart[index].number += value;
  }
  this.setState({
    cart: cloneCart,
  });
};
  render() {
    return (
      <div className='container'>
        <Cart cart={this.state.cart} handleUpdateQuantity={this.handleUpdateQuantity}/>
        <ListShoe 
        handleAddToCart={this.handleAddToCart}
        handleChangeDetail={this.handleChangeDetail}
        shoeArr={this.state.shoeArr}/>
        <DetailShoe detail={this.state.detail} />
      </div>
    )
  }
}
