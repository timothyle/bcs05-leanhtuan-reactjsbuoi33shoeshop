import React, { Component } from 'react'

export default class ItemShoe extends Component {
  render() {
    let { image,name,price } = this.props.data;
    return (
      <div className='col-3 p-1'>
        <div className='card text-left h-100'>
            <img className='card-img-top' src={image} alt="" />
            <div className='card-body'>
                <h4 className='card-title'>{name}</h4>
                <h5>${price}</h5>
                <button
                onClick={() => {
                this.props.handleAddItemToCart(this.props.data);
                }}
                className="btn btn-dark mr-4"
                >
                Add to Cart <i class="fas fa-shopping-cart"></i>
                </button>
                <button
                onClick={() => {
                this.props.handleViewDetail(this.props.data);
                }}
                className="btn btn-primary"
                >
                Detail
                </button>
            </div>
        </div>
      </div>
    )
  }
}
